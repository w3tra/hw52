import React, {Component} from 'react';
import './App.css';
import Number from "./components/Number/Number";

class App extends Component {
  state = {
    numbers: [...Array(37).keys()].splice(5,32),
    random: [0, 0, 0, 0, 0]
  };

  changeRandomNumbers = () => {
    let random = [...this.state.random];
    let shuffled = this.state.numbers.sort(() => .5 - Math.random());
    random =shuffled.slice(0,5);
    random.sort(function(a, b){return a - b});
    this.setState({random});
  };

  createNumbers = () => {
    const elements = [];
    this.state.random.forEach(function (number, i) {
      elements.push(<Number key={i} number={number}/>);
    });

    elements.push(<button type="button" key="button" onClick={this.changeRandomNumbers}>New</button>);

    return elements;
  };



  render() {
      return (
        this.createNumbers()
      );
  }
}

export default App;
