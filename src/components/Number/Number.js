import React, {Component} from "react"

import './Number.css';

class Number extends Component {
    render() {
        return (
            <div className="number">
                <p>{this.props.number}</p>
            </div>
        );
    }
}

export default Number;